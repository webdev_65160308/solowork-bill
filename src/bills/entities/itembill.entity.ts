export class ItemBill {
  id: number;
  quantity: number;
  description: string;
  unitPrice: number;
  amount: number;
}
