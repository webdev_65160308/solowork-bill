import { ItemBill } from './itembill.entity';

export class Bill {
  id: number;
  sellerInfo: string;
  buyerInfo: string;
  itemDetail: ItemBill;
  PayerSignature: string;
  PayeeSignature: string;
}
