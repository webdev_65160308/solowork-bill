import { Bill } from './bill.entity';

export class Bills {
  id: number;
  bill: Bill;
  total: number;
  totalString: string;
}
