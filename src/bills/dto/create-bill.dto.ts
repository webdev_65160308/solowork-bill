import { ItemBill } from '../entities/itembill.entity';

export class CreateBillDto {
  id: number;
  sellerInfo: string;
  buyerInfo: string;
  itemDetail: ItemBill;
  PayerSignature: string;
  PayeeSignature: string;
}
