import { Injectable } from '@nestjs/common';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bills } from './entities/bills.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBillDto } from './dto/create-bill.dto';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bills)
    private billsRepository: Repository<Bills>,
  ) {}
  create(createBillDto: CreateBillDto): Promise<Bills> {
    return this.billsRepository.save(createBillDto);
  }

  findAll() {
    return 'This action returns all bill';
  }

  findOne(id: number) {
    return `This action returns a #${id} bill`;
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.billsRepository.update(id, updateBillDto);
    const bill = await this.billsRepository.findOneBy({ id });
    return bill;
  }

  async remove(id: number) {
    const deleteBill = await this.billsRepository.findOneBy({ id });
    return this.billsRepository.remove(deleteBill);
  }
}
